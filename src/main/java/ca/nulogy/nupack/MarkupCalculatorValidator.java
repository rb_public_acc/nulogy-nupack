package ca.nulogy.nupack;

import java.math.BigDecimal;

/**
 * Created by Richard on 17-01-30.
 */
public class MarkupCalculatorValidator {

    public static final String INVALID_BASE_PRICE = "INVALID INPUT - BASE PRICE MAY ONLY CONTAIN ADDITIONAL DOLLAR SIGN OR COMMA CHARACTERS";
    public static final String INVALID_BASE_PRICE_LESS_THAN_ZERO = "INVALID INPUT - BASE PRICE MUST BE GREATER THAN ZERO";
    public static final String INVALID_NUM_OF_PEOPLE = "INVALID INPUT - NUMBER OF PEOPLE BE GREATER THAN ZERO AND MUST FOLLOW FORMAT 'X person' OR 'X people";
    public static final String NUM_OF_PEOPLE_NULL_INPUT = "INVALID INPUT - NUMBER OF PEOPLE MAY NOT BE NULL";
    public static final String BASEPRICE_NULL_INPUT = "INVALID INPUT - BASE PRICE MAY NOT BE NULL";
    public static final String INVALID_MATERIALS_TYPE = "INVALID INPUT - MATERIALS TYPE MAY NOT BE NULL";

    private String errorMessage;

    public String validateInput(String basePrice, String numOfPeopleOnJob, String materialsType) {
        if(inputIsValid(basePrice, numOfPeopleOnJob, materialsType)) {
            return null;
        } else {
            return this.errorMessage;
        }
    }

    private boolean inputIsValid(String basePrice, String numOfPeopleOnJob, String materialsType) {
        return basePriceIsValid(basePrice) && numOfPeopleOnJobIsValid(numOfPeopleOnJob) && materialsTypeIsValid(materialsType);
    }

    private boolean basePriceIsValid(String basePrice) {
        if(basePrice == null) {
            this.errorMessage = BASEPRICE_NULL_INPUT;
            return false;
        }

        try {
            BigDecimal validatedBasePriceInDecimal = new BigDecimal(MarkupCalculatorUtils.formatInputBasePrice(basePrice));
            return isValidValueGreaterThanZero(validatedBasePriceInDecimal);
        } catch (NumberFormatException ex) {
            this.errorMessage = INVALID_BASE_PRICE;
            return false;
        }
    }

    private boolean isValidValueGreaterThanZero(BigDecimal value) {
        if(value.compareTo(BigDecimal.ZERO) < 0) {
            this.errorMessage = INVALID_BASE_PRICE_LESS_THAN_ZERO;
            return false;
        }
        return true;
    }

    private boolean numOfPeopleOnJobIsValid(String numOfPeopleOnJob) {
        if(numOfPeopleOnJob == null) {
            this.errorMessage = NUM_OF_PEOPLE_NULL_INPUT;
            return false;
        }

        String[] toValidate = numOfPeopleOnJob.split(" ");
        if(toValidate.length != 2) {
            this.errorMessage = INVALID_NUM_OF_PEOPLE;
            return false;
        }

        if(!isValidNumberOfPeople(toValidate[0]) || !isValidPeopleTerminology(toValidate[1])) {
            this.errorMessage = INVALID_NUM_OF_PEOPLE;
            return false;
        }
        return true;
    }

    private boolean isValidNumberOfPeople(String numberOfPeople) {
        try {
            int numOfPeople = Integer.parseInt(numberOfPeople);
            if(numOfPeople <= 0) {
                return false;
            }

        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    private boolean isValidPeopleTerminology(String peopleTermUsed) {
        return peopleTermUsed.equalsIgnoreCase("people") || peopleTermUsed.equalsIgnoreCase("person");
    }

    private boolean materialsTypeIsValid(String materialsType) {
        if(materialsType == null) {
            this.errorMessage = INVALID_MATERIALS_TYPE;
            return false;
        }
        return true;
    }
}
