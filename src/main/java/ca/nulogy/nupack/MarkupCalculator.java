package ca.nulogy.nupack;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Richard on 17-01-29.
 */
public class MarkupCalculator {

    private static final BigDecimal FLAT_MARKUP_RATE = new BigDecimal(0.05);
    private static final BigDecimal PER_PERSON_MARKUP_RATE = new BigDecimal(0.012);

    public static final Map<String, String> materialsTypeMarkupMap = new HashMap<>();

    static {
        {
            materialsTypeMarkupMap.put("drugs", "0.075");
            materialsTypeMarkupMap.put("food", "0.13");
            materialsTypeMarkupMap.put("electronics", "0.02");
        }
    }

    private MarkupCalculatorValidator validator = new MarkupCalculatorValidator();
    private BigDecimal finalPrice;
    private BigDecimal additionalMarkups;

    public String calculateMarkup(String basePrice, String numOfPeopleOnJob, String materialsType) {
        String errorMessage = validator.validateInput(basePrice, numOfPeopleOnJob, materialsType);
        if (errorMessage != null) {
            return errorMessage;
        }

        calculateFlatRateMarkup(MarkupCalculatorUtils.formatInputBasePrice(basePrice));
        calculatePerPersonRateMarkup(MarkupCalculatorUtils.getNumberOfPeopleOnJob(numOfPeopleOnJob));
        calculateMaterialsTypeMarkup(materialsType);
        addAdditionalMarkupsToFlatRateMarkedUpPrice();

        return formattedMarkupPrice();
    }

    private void calculatePerPersonRateMarkup(int numOfPeopleOnJob) {
        BigDecimal totalPerPersonMarkup = PER_PERSON_MARKUP_RATE.multiply(new BigDecimal(numOfPeopleOnJob));
        this.additionalMarkups = finalPrice.multiply(totalPerPersonMarkup);
    }

    private void calculateFlatRateMarkup(String formattedBasePrice) {
        BigDecimal basePrice = new BigDecimal(formattedBasePrice);
        this.finalPrice = basePrice.add(basePrice.multiply(FLAT_MARKUP_RATE));
    }

    private void calculateMaterialsTypeMarkup(String materialsType) {
        if (materialsTypeMarkupMap.containsKey(materialsType)) {
            BigDecimal markupRateForMaterial = new BigDecimal(materialsTypeMarkupMap.get(materialsType));
            BigDecimal markup = finalPrice.multiply(markupRateForMaterial);
            additionalMarkups = additionalMarkups.add(markup);
        }
    }

    private void addAdditionalMarkupsToFlatRateMarkedUpPrice() {
        finalPrice = finalPrice.add(additionalMarkups);
    }

    private String formattedMarkupPrice() {
        return String.format("$%,.2f", finalPrice.setScale(2, RoundingMode.HALF_UP));
    }

}