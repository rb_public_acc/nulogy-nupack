package ca.nulogy.nupack;

/**
 * Created by Richard on 17-01-30.
 */
public class MarkupCalculatorUtils {

    public static String formatInputBasePrice(String inputBasePrice) {
        return inputBasePrice.replaceAll(",", "").replaceAll("\\$", "");
    }

    public static int getNumberOfPeopleOnJob(String numberOfPeopleOnJob) {
        String[] numberOfPeopleWithTerminology = numberOfPeopleOnJob.split(" ");
        return Integer.parseInt(numberOfPeopleWithTerminology[0]);
    }
}
