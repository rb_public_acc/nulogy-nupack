package ca.nulogy.nupack;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Richard on 17-01-30.
 */
public class MarkupCalculatorUtilsTest {

    private static String PRICE_WITH_DOLLAR_SIGN = "$1234";
    private static String PRICE_WITH_COMMA = "1,234";
    private static String PRICE_WITH_DOLLAR_AND_COMMA = "$1,234";
    private static String EXPECTED_VALID_OUTPUT = "1234";
    private static String VALID_NUMBER_OF_PEOPLE_INPUT = "3 people";
    private static String INVALID_NUMBER_OF_PEOPLE_INPUT = "people";

    @Test
    public void test_whenGivenAStringWithADollarSign_returnsStringWithoutDollarSign() {
        String returnedValue = MarkupCalculatorUtils.formatInputBasePrice(PRICE_WITH_DOLLAR_SIGN);
        assertNotNull(returnedValue);
        assertEquals(EXPECTED_VALID_OUTPUT, returnedValue);
    }

    @Test
    public void test_whenGivenAStringWithCommas_returnsStringWithoutCommas() {
        String returnedValue = MarkupCalculatorUtils.formatInputBasePrice(PRICE_WITH_COMMA);
        assertNotNull(returnedValue);
        assertEquals(EXPECTED_VALID_OUTPUT, returnedValue);
    }

    @Test
    public void test_whenGivenAStringWithCommasAndDollarSigns_returnsStringWithoutCommasOrDollarSigns() {
        String returnedValue = MarkupCalculatorUtils.formatInputBasePrice(PRICE_WITH_DOLLAR_AND_COMMA);
        assertNotNull(returnedValue);
        assertEquals(EXPECTED_VALID_OUTPUT, returnedValue);
    }

    @Test
    public void test_whenGivenAStringWithNumberOfPeople_returnsNumberOfPeopleAsInteger() {
        int returnedValue = MarkupCalculatorUtils.getNumberOfPeopleOnJob(VALID_NUMBER_OF_PEOPLE_INPUT);
        assertNotNull(returnedValue);
        assertEquals(3, returnedValue);
    }

    @Test(expected = NumberFormatException.class)
    public void test_whenGivenAStringWithNoNumberOfPeople_throwsNumberFormatException() {
        MarkupCalculatorUtils.getNumberOfPeopleOnJob(INVALID_NUMBER_OF_PEOPLE_INPUT);
    }

}
