package ca.nulogy.nupack;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Richard on 17-01-30.
 */
public class MarkupCalculatorValidatorTest {

    private static final String VALID_BASE_PRICE = "$1,299.99";
    private static final String INVALID_BASE_PRICE = "&1299.00";
    private static final String INVALID_BASE_PRICE_LESS_THAN_ZERO = "-$2,199.00";
    private static final String VALID_THREE_PEOPLE = "3 people";
    private static final String INVALID_NUMBER_OF_PEOPLE_1 = "one person";
    private static final String INVALID_NUMBER_OF_PEOPLE_2 = "1 cat";
    private static final String INVALID_NUMBER_OF_PEOPLE_3 = "-2 people";

    private static final String MATERIALS_FOOD = "food";

    private MarkupCalculatorValidator markupCalculatorValidator;

    @Before
    public void setup() {
        markupCalculatorValidator = new MarkupCalculatorValidator();
    }

    @Test
    public void test_whenGivenAnInvalidBasePrice_correctErrorMessageIsReturned() {
        String returnedValue = markupCalculatorValidator.validateInput(INVALID_BASE_PRICE, VALID_THREE_PEOPLE, MATERIALS_FOOD);
        assertNotNull(returnedValue);
        assertEquals(MarkupCalculatorValidator.INVALID_BASE_PRICE, returnedValue);
    }

    @Test
    public void test_whenGivenABasePriceLessThanZero_correctErrorMessageIsReturned() {
        String returnedValue = markupCalculatorValidator.validateInput(INVALID_BASE_PRICE_LESS_THAN_ZERO, VALID_THREE_PEOPLE, MATERIALS_FOOD);
        assertNotNull(returnedValue);
        assertEquals(MarkupCalculatorValidator.INVALID_BASE_PRICE_LESS_THAN_ZERO, returnedValue);
    }

    @Test
    public void test_whenGivenANullBasePrice_correctErrorMessageIsReturned() {
        String returnedValue = markupCalculatorValidator.validateInput(null, VALID_THREE_PEOPLE, MATERIALS_FOOD);
        assertNotNull(returnedValue);
        assertEquals(MarkupCalculatorValidator.BASEPRICE_NULL_INPUT, returnedValue);
    }

    @Test
    public void test_whenGivenAnInvalidNumberOfPeople1_correctErrorMessageIsReturned() {
        String returnedValue = markupCalculatorValidator.validateInput(VALID_BASE_PRICE, INVALID_NUMBER_OF_PEOPLE_1, MATERIALS_FOOD);
        assertNotNull(returnedValue);
        assertEquals(MarkupCalculatorValidator.INVALID_NUM_OF_PEOPLE, returnedValue);
    }

    @Test
    public void test_whenGivenAnInvalidNumberOfPeople2_correctErrorMessageIsReturned() {
        String returnedValue = markupCalculatorValidator.validateInput(VALID_BASE_PRICE, INVALID_NUMBER_OF_PEOPLE_2, MATERIALS_FOOD);
        assertNotNull(returnedValue);
        assertEquals(MarkupCalculatorValidator.INVALID_NUM_OF_PEOPLE, returnedValue);
    }

    @Test
    public void test_whenGivenANegativeNumberOfPeople_correctErrorMessageIsReturned() {
        String returnedValue = markupCalculatorValidator.validateInput(VALID_BASE_PRICE, INVALID_NUMBER_OF_PEOPLE_3, MATERIALS_FOOD);
        assertNotNull(returnedValue);
        assertEquals(MarkupCalculatorValidator.INVALID_NUM_OF_PEOPLE, returnedValue);
    }

    @Test
    public void test_whenGivenANullNumberOfPeople_correctErrorMessageIsReturned() {
        String returnedValue = markupCalculatorValidator.validateInput(VALID_BASE_PRICE, null, MATERIALS_FOOD);
        assertNotNull(returnedValue);
        assertEquals(MarkupCalculatorValidator.NUM_OF_PEOPLE_NULL_INPUT, returnedValue);
    }

    @Test
    public void test_whenGivenANullMaterialsType_correctErrorMessageIsReturned() {
        String returnedValue = markupCalculatorValidator.validateInput(VALID_BASE_PRICE, VALID_THREE_PEOPLE, null);
        assertNotNull(returnedValue);
        assertEquals(MarkupCalculatorValidator.INVALID_MATERIALS_TYPE, returnedValue);
    }

}
