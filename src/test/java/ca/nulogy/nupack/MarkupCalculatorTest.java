package ca.nulogy.nupack;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by Richard on 17-01-29.
 */
public class MarkupCalculatorTest {

    private static String VALID_BASEPRICE_INPUT_1 = "$1,299.99";
    private static String VALID_NUMOFPEOPLE_INPUT_1 = "3 people";
    private static String VALID_MATERIALTYPE_INPUT_1 = "food";
    private static String EXPECTED_OUTPUT_1 = "$1,591.58";

    private static String VALID_BASEPRICE_INPUT_2 = "$5,432.00";
    private static String VALID_NUMOFPEOPLE_INPUT_2 = "1 person";
    private static String VALID_MATERIALTYPE_INPUT_2 = "drugs";
    private static String EXPECTED_OUTPUT_2 = "$6,199.81";

    private static String VALID_BASEPRICE_INPUT_3 = "$12,456.95";
    private static String VALID_NUMOFPEOPLE_INPUT_3 = "4 people";
    private static String VALID_MATERIALTYPE_INPUT_3 = "books";
    private static String EXPECTED_OUTPUT_3 = "$13,707.63";

    private static String INVALID_BASEPRICE_INPUT = "&1299.99";
    private static String INVALID_NUMOFPEOPLE_INPUT = "people";
    private static String INVALID_BASEPRICE_LESS_THAN_ZERO = "-$1,299.99";

    private MarkupCalculator markupCalculator;

    @Before
    public void setup() {
        this.markupCalculator = new MarkupCalculator();
    }

    @Test
    public void test_whenGivenValidPriceNumOfPeopleAndMaterials_returnsCorrectMarkupPrice() {
        String returnedValue1 = markupCalculator.calculateMarkup(VALID_BASEPRICE_INPUT_1, VALID_NUMOFPEOPLE_INPUT_1, VALID_MATERIALTYPE_INPUT_1);
        assertNotNull(returnedValue1);
        assertEquals(EXPECTED_OUTPUT_1, returnedValue1);

        String returnedValue2 = markupCalculator.calculateMarkup(VALID_BASEPRICE_INPUT_2, VALID_NUMOFPEOPLE_INPUT_2, VALID_MATERIALTYPE_INPUT_2);
        assertNotNull(returnedValue2);
        assertEquals(EXPECTED_OUTPUT_2, returnedValue2);

        String returnedValue = markupCalculator.calculateMarkup(VALID_BASEPRICE_INPUT_3, VALID_NUMOFPEOPLE_INPUT_3, VALID_MATERIALTYPE_INPUT_3);
        assertNotNull(returnedValue);
        assertEquals(EXPECTED_OUTPUT_3, returnedValue);
    }

    @Test
    public void test_whenGivenInvalidInput_returnsCorrectErrorMessages() {
        String returnedValue1 = markupCalculator.calculateMarkup(INVALID_BASEPRICE_INPUT, VALID_NUMOFPEOPLE_INPUT_1, VALID_MATERIALTYPE_INPUT_1);
        assertNotNull(returnedValue1);
        assertEquals(MarkupCalculatorValidator.INVALID_BASE_PRICE, returnedValue1);

        String returnedValue2 = markupCalculator.calculateMarkup(VALID_BASEPRICE_INPUT_1, INVALID_NUMOFPEOPLE_INPUT, VALID_MATERIALTYPE_INPUT_1);
        assertNotNull(returnedValue2);
        assertEquals(MarkupCalculatorValidator.INVALID_NUM_OF_PEOPLE, returnedValue2);

        String returnedValue3 = markupCalculator.calculateMarkup(VALID_BASEPRICE_INPUT_1, VALID_NUMOFPEOPLE_INPUT_1, null);
        assertNotNull(returnedValue3);
        assertEquals(MarkupCalculatorValidator.INVALID_MATERIALS_TYPE, returnedValue3);

        String returnedValue4 = markupCalculator.calculateMarkup(INVALID_BASEPRICE_LESS_THAN_ZERO, VALID_NUMOFPEOPLE_INPUT_1, null);
        assertNotNull(returnedValue4);
        assertEquals(MarkupCalculatorValidator.INVALID_BASE_PRICE_LESS_THAN_ZERO, returnedValue4);
    }

}