# Nulogy Nupack Markup Calculator

## To Install Dependencies & Build:
`./gradlew clean build` (from project root)

## To Test:
`./gradlew test`